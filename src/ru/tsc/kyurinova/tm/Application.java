package ru.tsc.kyurinova.tm;

import ru.tsc.kyurinova.tm.constant.TerminalConst;

import java.util.Arrays;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if(args == null || args.length == 0) return;
        final String arg = args[0];
        if(TerminalConst.ABOUT.equals(arg)) showAbout();
        if(TerminalConst.VERSION.equals(arg)) showVersion();
        if(TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Kseniia Yurinova");
        System.out.println("E-MAIL: ks093050@mail.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Display developer info...");
        System.out.println(TerminalConst.VERSION + " - Display program version...");
        System.out.println(TerminalConst.HELP + " - Display list of commands...");
    }

}
